module App

open Elmish
open Elmish.React
open Elmish.ReactNative
open Fable.ReactNative

open Fable.Core
open Fable.Core.JsInterop
open Fable.SimpleJson

module R = Fable.ReactNative.Helpers
module P = Fable.ReactNative.Props
open Fable.ReactNative.Props

open AsyncStorage

open Sounds


[<ImportDefault("@react-native-async-storage/async-storage")>]
let asyncStorage: AsyncStorage = jsNative

[<RequireQualifiedAccess>]
type Theme = Fable.ReactNative.Types.ColorScheme

type Model = { Theme: Theme; }

type Msg =
    | SwitchTheme of Theme
    | LoadModel of Model

let loadFromStorage () =
    Cmd.ofSub (fun dispatch ->
        promise {
            try
                let! storedModel = asyncStorage.getItem "model"
                let parsedModel: Model = Json.parseAs<Model> storedModel
                Msg.LoadModel parsedModel |> dispatch
            with
            | err ->
                printfn "error: %A" err
        } |> Promise.start
    )

let addThemeListener () =
    Cmd.ofSub (fun dispatch ->
        RN.Appearance.addChangeListener (fun appearance ->
            appearance.colorScheme
            |> Msg.SwitchTheme
            |> dispatch
        )
    )

let init () =
    RN.StatusBar.setHidden true Types.StatusBarAnimation.Slide //? disable status bar

    { Theme = RN.Appearance.getColorScheme() },
    Cmd.batch [
        loadFromStorage()
        addThemeListener()
    ]

let update msg model =
    let newModel =
        match msg with
        | SwitchTheme theme ->
            let newModel = { model with Theme = theme }

            try
                asyncStorage.setItem "model" (Json.serialize newModel) |> ignore
                printfn "New model saved !"
            with
            | err -> printfn "Error when save model ! -> %A" err

            newModel

        | LoadModel loadedModel ->
            loadedModel

    newModel, Cmd.none

let view model dispatch =
    R.view [
        P.ViewProperties.Style [
            P.FlexStyle.Flex 1.
            P.FlexStyle.AlignItems ItemAlignment.Center

            (
                if model.Theme = Theme.Dark then
                    P.BackgroundColor "#141C1F"
                else
                    P.BackgroundColor "#ffffff"
            )
        ]
    ] [

        R.scrollView [
            P.ViewProperties.Style [
                P.ZIndex 1.
                Width ( R.pct 100. )
            ]

            P.ScrollViewProperties.ContentContainerStyle [
                P.FlexStyle.AlignItems ItemAlignment.Center
            ]
        ] [

            // Switch.switch
            //     (if model.Theme = Theme.Dark then false elif model.Theme = Theme.Light then true else false)
            //     [ P.ZIndex 2.
            //       Position Position.Absolute
            //       P.FlexStyle.Top (R.dip 10.)
            //       P.FlexStyle.Right (R.dip 10.) ]
            //     (fun _ -> (if model.Theme = Theme.Dark then Theme.Light |> Msg.SwitchTheme else Theme.Dark |> Msg.SwitchTheme) |> dispatch)

            MyButton.titleButton (fun _ -> playOrStopSound Sounds.dejaVu)

            R.view [
                P.ViewProperties.Style [
                    Width ( R.pct 100. )

                    P.FlexStyle.JustifyContent JustifyContent.SpaceEvenly
                ]
            ] (allSoundPacks |> List.map (fun soundPacks ->
                R.view [
                    P.ViewProperties.Style [
                        P.FlexStyle.FlexDirection FlexDirection.Row
                        P.FlexStyle.JustifyContent JustifyContent.SpaceEvenly
                    ]
                ] (soundPacks |> List.map (fun sound ->
                    MyButton.SoundBoxButton {
                        Image = sound.Image;
                        Height = (R.dip 80.);
                        Width = (R.dip 80.);
                        OnPress = (fun _ ->
                            sound |> playOrStopSound
                        )
                        Position = sound.Position;
                    }
                )))
            )
        ]
    ]

Program.mkProgram init update view
|> Program.withConsoleTrace
|> Program.withReactNative "SoundBox"
|> Program.run