module MyButton

open Fable.ReactNative
open LinearGradient


module R = Fable.ReactNative.Helpers
module P = Fable.ReactNative.Props
open Fable.ReactNative.Props

//? Center element with margin
let marginForCenter (elementWidth: float) =
    let screenWidth = RN.Dimensions.get("window").width
    screenWidth / 2. - elementWidth / 2.

let buttonStyle : IStyle list =
    [ //? Flex
      P.FlexStyle.AlignItems ItemAlignment.Center
      P.FlexStyle.JustifyContent JustifyContent.Center

      //? Shadow
      P.Elevation 10.
      P.ShadowOffset { width = 0.; height = 12. }
      P.ShadowOpacity 0.58
      P.ShadowColor "black"
      P.ShadowRadius 16.

      //? Dimensions
      P.FlexStyle.Width (R.dip (RN.Dimensions.get("window").width / 3. - 24.))
      P.FlexStyle.Height (R.dip (RN.Dimensions.get("window").height / 8. - 12.))

      P.MarginVertical(R.dip 12.5)
      P.BorderRadius 23. ]

type ButtonOptions =
    { Image: IImageSource
      Width: ISizeUnit
      Height: ISizeUnit
      OnPress: unit -> unit
      Position: Sounds.ButtonPosition }



type GradProps = LinearGradient.LinearGradientProps

let titleButton onPress =
    R.touchableWithoutFeedback [ OnPress onPress; ] [
        linearGradient [
            GradProps.Style [
               //? Flex
               P.FlexStyle.AlignItems ItemAlignment.Center
               P.FlexStyle.JustifyContent JustifyContent.Center

               //? Shadow
               P.Elevation 10.
               P.ShadowOffset { width = 0.; height = 12. }
               P.ShadowOpacity 0.58
               P.ShadowColor "black"
               P.ShadowRadius 16.

               //? Dimensions
               P.FlexStyle.Width (R.dip 230.)
               P.FlexStyle.Height (R.dip 55.)

               P.MarginTop (R.dip 120.)
               P.MarginBottom (R.dip 120.)
               P.MarginLeft (R.dip (marginForCenter 230.))

               P.BorderRadius 22. ]
            GradProps.Colors [| "#64DFDF"; "#48BFE3" |]
        ] [
            R.text [
                P.TextProperties.Style [
                    P.Color "#151A1C"
                    FontFamily "OpenSans-Bold"
                    FontSize 24.
                ]
            ] "SoundBox"
        ]
    ]

let SoundBoxButton options =
    let mutable gradStartPosition: Vector2 = { x = 0; y = 0}
    let mutable gradEndPosition: Vector2 = { x = 0; y = 1}

    match options.Position with
    | Sounds.ButtonPosition.Left ->
        gradStartPosition <- { x = 0; y = 0 }
        gradEndPosition <- { x = 1; y = 1 }
    | Sounds.ButtonPosition.Middle ->
        gradStartPosition <- { x = 0; y = 0 }
        gradEndPosition <- { x = 0; y = 1 }
    | Sounds.ButtonPosition.Right ->
        gradStartPosition <- { x = 1; y = 0 }
        gradEndPosition <- { x = 0; y = 1 }

    R.touchableWithoutFeedback [ OnPress options.OnPress ] [

        linearGradient [
            GradProps.Style buttonStyle
            GradProps.Colors [| "#64DFDF"; "#48BFE3" |]

            GradProps.Start gradStartPosition
            GradProps.End gradEndPosition
        ] [
            R.image [
                Source options.Image
                P.ImageProperties.Style [
                    Width(R.dip 50.)
                    Height(R.dip 50.)
                ]
            ]
        ]
    ]
