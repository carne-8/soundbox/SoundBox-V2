module ReactAnimation

open Fable.Core
open Fable.Core.JsInterop


type Animation = float
type AnimationFunction =
    abstract start: ?callback: (bool -> unit) -> unit

type TimingOptions =
    { toValue: float
      duration: int
      useNativeDriver: bool }

type Animated =
    [<Emit("new $0.Value($1)")>]
    abstract member Value: float -> Animation
    abstract member timing: Animation * TimingOptions -> AnimationFunction

[<Import("Animated", "react-native")>]
let Animated: Animated = jsNative