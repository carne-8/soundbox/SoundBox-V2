module SoundBinding

open Fable.Core
open Fable.Core.JsInterop

type Sound =
    [<Emit("new $0($1)")>]
    abstract createSound: name: string -> Sound
    abstract play: ?callBack: (unit -> unit) -> unit
    abstract stop: ?callBack: (unit -> unit) -> unit
    abstract setVolume: float -> unit